## VK..OK...AND?
Нужно найти пользака на платформе Мой Мир

RU: Тогда эта социальная сеть была очень популярной в России!

Найди страничку котика

Нам известно лишь то, что страничка зарегистрирована на почту zer0xzer0@mail.ru

EU: Then this social network was very popular in Russia!

Find the cat's page

We only know that the page is registered to the mail zer0xzer0@mail.ru

## Writeups 
![image.png](./image.png)
![image.png](./image2.png)

## Flag
MCTF{oh..my_woRrrR1Ld}

## Сложность
Easy
